package DecBinHexdec;

/**
 * Created by adam12 on 08.04.18.
 */
public interface Conversion {
    String convertDecToBin(String dec);

    String convertDecToHexDec(String dec);

    String convertBinToHexDec(String bin);

    String convertBinToDec(String bin);

    String convertHexDecToDec(String hexd);

    String convertHexDecToBin(String hexd);

    Number convertStringToInt(String string);


}
