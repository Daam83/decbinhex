package DecBinHexdec;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Created by adam12 on 12.04.18.
 */
public class StartDecBitHexDec extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("DecBinHex");

        Parent layout = FXMLLoader.load(getClass().getResource("Gui.fxml"));

        Scene scene = new Scene(layout);
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("DecBinHexdec_small.ico")));

        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
