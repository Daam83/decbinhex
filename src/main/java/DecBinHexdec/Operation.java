package DecBinHexdec;

/**
 * Created by adam12 on 03.04.18.
 */
public class Operation implements Conversion {

    @Override
    public String convertDecToBin(String dec) {
        String bin = "";
        int integer = Integer.valueOf(dec);
        do {
            int binval;
            if (integer % 2 == 1) {
                binval = 1;
            } else {
                binval = 0;
            }

            bin = binval + bin;
            integer = (integer / 2);
        }
        while (integer > 0);

        return bin;
    }

    @Override
    public String convertDecToHexDec(String dec) {
        String HexDec = "";
        int integer = Integer.valueOf(dec);
        do {
            String hexdecval = "";
            int resultModul = integer % 16;
            if (resultModul > 9) {
                switch (resultModul) {
                    case 10:
                        hexdecval = "A";
                        break;
                    case 11:
                        hexdecval = "B";
                        break;
                    case 12:
                        hexdecval = "C";
                        break;
                    case 13:
                        hexdecval = "D";
                        break;
                    case 14:
                        hexdecval = "E";
                        break;
                    case 15:
                        hexdecval = "F";
                        break;
                }
            } else {
                hexdecval = resultModul + hexdecval;
            }
            HexDec = hexdecval + HexDec;
            integer = (integer / 16);

        } while (integer > 0);

        return HexDec;
    }

    @Override
    public String convertBinToHexDec(String bin) {
        return convertDecToHexDec(convertBinToDec(bin));
    }

    @Override
    public String convertBinToDec(String bin) {
        String dec = "";
        int decval = 0;
        for (int i = 0; i < bin.length(); i++) {
            decval = (int) (decval + Character.getNumericValue(bin.charAt(bin.length() - 1 - i)) * Math.pow(2, i));
        }

        return dec + decval;

    }

    @Override
    public String convertHexDecToDec(String hexd) {
        String dec = "";
        int decval = 0;
        int val = 0;
        for (int i = 0; i < hexd.length(); i++) {
            switch (hexd.charAt(hexd.length() - i - 1)) {
                case 'A':
                    decval = 10;
                    break;
                case 'B':
                    decval = 11;
                    break;
                case 'C':
                    decval = 12;
                    break;
                case 'D':
                    decval = 13;
                    break;
                case 'E':
                    decval = 14;
                    break;
                case 'F':
                    decval = 15;
                    break;
                default:
                    decval = Character.getNumericValue(hexd.charAt(hexd.length() - i - 1));
                    break;
            }
            val = val + (int) (decval * Math.pow(16, i));
        }

        return dec + val;
    }

    @Override
    public String convertHexDecToBin(String hexd) {
        return convertDecToBin(convertHexDecToDec(hexd));
    }

    @Override
    public Number convertStringToInt(String string) {
        return Integer.valueOf(string);
    }

}
