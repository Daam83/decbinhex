package DecBinHexdec;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * Created by adam12 on 12.04.18.
 */
public class EngineGui {
    Operation operation = new Operation();

    @FXML
    private TextField hexTextField;
    @FXML
    private TextField decTextField;
    @FXML
    private TextField binTextField;

    public String getHexTextField() {
        return hexTextField.getText();
    }

    public void setHexTextField(String SetText) {
        this.hexTextField.setText(SetText);
    }

    public String getDecTextField() {
        return decTextField.getText();
    }

    public void setDecTextField(String SetText) {
        this.decTextField.setText(SetText);
    }

    public String getBinTextField() {
        return binTextField.getText();
    }

    public void setBinTextField(String SetText) {
        this.binTextField.setText(SetText);
    }

    @FXML
    public void hexaChange(KeyEvent event) {
        if (checkInputEvent(this.hexTextField, event)) {
            hexaToUpperCase(this.hexTextField, event);
            setDecTextField(operation.convertHexDecToDec(hexTextField.getText()));
            setBinTextField(operation.convertHexDecToBin(hexTextField.getText()));
        }
    }

    @FXML
    public void decChange(KeyEvent event) {
        if (checkInputEvent(this.decTextField, event)) {
            setBinTextField(operation.convertDecToBin(getDecTextField()));
            setHexTextField(operation.convertDecToHexDec(getDecTextField()));
        }
    }

    @FXML
    public void binChange(KeyEvent event) {
        if (checkInputEvent(this.binTextField, event)) {
            setDecTextField(operation.convertBinToDec(getBinTextField()));
            setHexTextField(operation.convertBinToHexDec(getBinTextField()));
        }
    }

    private void hexaToUpperCase(TextField updateTextField, KeyEvent event) {
        String hexaText = updateTextField.getText();
        hexaText = hexaText.toUpperCase();
        int caretPosition = updateTextField.getCaretPosition();
        hexTextField.setText(hexaText);
        hexTextField.positionCaret(caretPosition);
    }

    private boolean checkInputEvent(TextField updateTextField, KeyEvent event) {
        if ((event.getCode() == KeyCode.ENTER) ||
                (event.getCode() == KeyCode.DELETE) ||
                (event.getCode() == KeyCode.BACK_SPACE) ||
                (event.getCode() == KeyCode.ENTER) ||
                (event.getCode() == KeyCode.SHIFT) ||
                (event.getCode() == KeyCode.ESCAPE) ||
                (event.getCode() == KeyCode.UP) ||
                (event.getCode() == KeyCode.DOWN) ||
                (event.getCode() == KeyCode.LEFT) ||
                (event.getCode() == KeyCode.RIGHT) ||
                (event.getCode() == KeyCode.TAB) ||
                (event.getCode() == KeyCode.HOME) ||
                (event.getCode() == KeyCode.END) ||
                (event.getCode() == KeyCode.INSERT)
                ) {
            return true;
        } else if (updateTextField.getId().equals("binTextField") &&
                (
                        (event.getCode() == KeyCode.DIGIT0 || event.getCode() == KeyCode.DIGIT1)
                )
                ) {
            return true;
        } else if ((updateTextField.getId().equals("decTextField")) &&
                (event.getCode().isDigitKey())) {
            return true;
        } else if ((updateTextField.getId().equals("hexTextField")) &&
                (
                        (event.getCode().isDigitKey()) ||
                                (event.getCode() == KeyCode.A) ||
                                (event.getCode() == KeyCode.B) ||
                                (event.getCode() == KeyCode.C) ||
                                (event.getCode() == KeyCode.D) ||
                                (event.getCode() == KeyCode.E) ||
                                (event.getCode() == KeyCode.F))
                ) {
            return true;
        } else {
            updateTextField.getId();
            String valueText = updateTextField.getText();
            String valueTextCorrected = valueText.replace(event.getText(), "");
            int caret = 0;
            updateTextField.setText(valueTextCorrected);
            for (int i = 0; i < valueText.length(); i++) {
                if (event.getText().equals(Character.toString(valueText.charAt(i)))) {
                    caret = i;
                    updateTextField.positionCaret(caret);
                }
            }
            return false;
        }
    }

}
