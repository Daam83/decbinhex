import DecBinHexdec.Operation;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by adam12 on 03.04.18.
 */
public class TestLogical {


    String[] decimal = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"};
    String[] bin = {"0", "1", "10", "11", "100", "101", "110", "111", "1000", "1001", "1010", "1011", "1100", "1101",
            "1110", "1111"};
    String[] hexa = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
    Operation operation = new Operation();

    // Decimal Bin Converter
    @Test
    public void Convert_decimal_to_Bin_String() {
        for (int i = 0; i < 16; i++) {
            Assert.assertEquals(bin[i], operation.convertDecToBin(decimal[i]));
        }
    }

    // Decimal Hexa Converter
    @Test
    public void Convert_Decimal_to_Hexadecimal_String() {
        for (int i = 0; i < 16; i++) {
            Assert.assertEquals(hexa[i], operation.convertDecToHexDec(decimal[i]));
        }
    }

    //Bin to decimal
    @Test
    public void Convert_Bin_to_Decimal_String() {
        for (int i = 0; i < 16; i++) {
            Assert.assertEquals(decimal[i], operation.convertBinToDec(bin[i]));
        }
    }

    //bin to hexa
    @Test
    public void Convert_in_to_Bin_Hexa_String() {
        for (int i = 0; i < 16; i++) {
            Assert.assertEquals(hexa[i], operation.convertBinToHexDec(bin[i]));
        }
    }

    //Hexa to bin
    @Test
    public void Convert_in_to_Hexa_Bin_String() {
        for (int i = 0; i < 16; i++) {
            Assert.assertEquals(bin[i], operation.convertHexDecToBin(hexa[i]));
        }
    }
    @Test
    public void Convert_in_to_Hexa_decimal_String() {
        for (int i = 0; i < 16; i++) {
            Assert.assertEquals(decimal[i], operation.convertHexDecToDec(hexa[i]));
        }
    }
}
